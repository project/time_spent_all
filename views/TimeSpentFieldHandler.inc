<?php

/**
 * @file
 * Definition of TimeSpentFieldHandler.
 */

/**
 * A handler to provide proper display for timespent length.
 *
 * @ingroup views_field_handlers
 */
class TimeSpentFieldHandler extends views_handler_field {

  /**
   * Responsible for rendering the custom field.
   *
   * @override
   */
  public function render($values) {
    $value = $this->get_value($values);
    return time_spent_sec2hms($value);
  }

}
